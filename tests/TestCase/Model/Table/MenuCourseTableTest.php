<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MenuCourseTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MenuCourseTable Test Case
 */
class MenuCourseTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MenuCourseTable
     */
    public $MenuCourse;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.menu_course',
        'app.menu_categories'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MenuCourse') ? [] : ['className' => 'App\Model\Table\MenuCourseTable'];
        $this->MenuCourse = TableRegistry::get('MenuCourse', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MenuCourse);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
