<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\MenuCategoriesTable;

/**
 * MenuCategories Controller
 *
 * @property \App\Model\Table\MenuCategoriesTable $MenuCategories
 */
class MenuCategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $menuCategories = $this->paginate($this->MenuCategories);

        $this->set(compact('menuCategories'));
        $this->set('_serialize', ['menuCategories']);
    }

    /**
     * View method
     *
     * @param string|null $id Menu Category id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menuCategory = $this->MenuCategories->get($id, [
            'contain' => []
        ]);

        $this->set('menuCategory', $menuCategory);
        $this->set('_serialize', ['menuCategory']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menuCategory = $this->MenuCategories->newEntity();
        if ($this->request->is('post')) {
            $menuCategory = $this->MenuCategories->patchEntity($menuCategory, $this->request->data);
            $menuCategory->set(['created_at' => $this->getDateTime()]);
            if ($this->MenuCategories->save($menuCategory)) {
                $this->Flash->success(__('The menu category has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The menu category could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('menuCategory'));
        $this->set('_serialize', ['menuCategory']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu Category id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menuCategory = $this->MenuCategories->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menuCategory = $this->MenuCategories->patchEntity($menuCategory, $this->request->data);
            $menuCategory->set(['created_at' => $this->getDateTime()]);
            if ($this->MenuCategories->save($menuCategory)) {
                $this->Flash->success(__('The menu category has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The menu category could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('menuCategory'));
        $this->set('_serialize', ['menuCategory']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu Category id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menuCategory = $this->MenuCategories->get($id);
        if ($this->MenuCategories->delete($menuCategory)) {
            $this->Flash->success(__('The menu category has been deleted.'));
        } else {
            $this->Flash->error(__('The menu category could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

    private function getDateTime(){
        return date('Y-m-d').' '.date('H:i:s');
    }
}
