<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use App\Model\Entity\MenuCategory;
use App\Model\Entity\MenuItem;
use App\Model\Entity\Reservation;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\View\Exception\MissingTemplateException;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function initialize(){
        parent::initialize();
        $this->viewBuilder()->layout('fable');
    }

    public function index(){

    }

    public function about(){

    }

    public function menu(){
//        $this->loadModel('MenuCategories');
        $query = TableRegistry::get('MenuCategories')->find();
        $categories = $query->select(['id', 'cat_name', 'cat_description'])->toArray();

        $query = TableRegistry::get('MenuCourse')->find();
        $courses = $query->select(['cat_id','course_id' => 'id', 'course_type', 'course_description', 'course_price', 'course_featured_image'])->toArray();

        $query = TableRegistry::get('MenuItems')->find();
        $items = $query->select(['item_id' => 'id', 'item_name', 'item_price', 'item_description', 'item_featured_image'])->toArray();
//        debug($courses);die();
        $this->set(compact('categories', 'courses', 'items'));
    }

    public function reservations(){
        $reservationModel = new Reservation();
//        debug($reservationModel);die();
        $this->set(compact('reservationModel'));
    }

    public function contact(){

    }
}
