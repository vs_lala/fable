<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * MenuCourse Controller
 *
 * @property \App\Model\Table\MenuCourseTable $MenuCourse
 */
class MenuCourseController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['MenuCategories']
        ];
        $menuCourse = $this->paginate($this->MenuCourse);

        $this->set(compact('menuCourse'));
        $this->set('_serialize', ['menuCourse']);
    }

    /**
     * View method
     *
     * @param string|null $id Menu Course id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $menuCourse = $this->MenuCourse->get($id, [
            'contain' => ['MenuCategories']
        ]);

        $this->set('menuCourse', $menuCourse);
        $this->set('_serialize', ['menuCourse']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menuCourse = $this->MenuCourse->newEntity();
        if ($this->request->is('post')) {
            $menuCourse = $this->MenuCourse->patchEntity($menuCourse, $this->request->data);
            if ($this->MenuCourse->save($menuCourse)) {
                $this->Flash->success(__('The menu course has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The menu course could not be saved. Please, try again.'));
            }
        }

        $query = TableRegistry::get('MenuCategories')->find();
        $menuCategories = $query->select(['id', 'cat_name'])->toArray();
//        debug($menuCategories);die();
        $this->set(compact('menuCourse', 'menuCategories'));
        $this->set('_serialize', ['menuCourse']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu Course id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menuCourse = $this->MenuCourse->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $menuCourse = $this->MenuCourse->patchEntity($menuCourse, $this->request->data);
            if ($this->MenuCourse->save($menuCourse)) {
                $this->Flash->success(__('The menu course has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The menu course could not be saved. Please, try again.'));
            }
        }
        $menuCategories = $this->MenuCourse->MenuCategories->find('list', ['limit' => 200]);
        $this->set(compact('menuCourse', 'menuCategories'));
        $this->set('_serialize', ['menuCourse']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Menu Course id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $menuCourse = $this->MenuCourse->get($id);
        if ($this->MenuCourse->delete($menuCourse)) {
            $this->Flash->success(__('The menu course has been deleted.'));
        } else {
            $this->Flash->error(__('The menu course could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
