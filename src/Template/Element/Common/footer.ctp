<footer>
    <div class="container">
        <div class="row">
            <div class="col s12 m3 l3">
                <h4 class="footer-title">About Us</h4>
                <p>Quis autem velis et reprehender etims quiste voluptate velit esse quam nihil etsa illum sedit consequatur quias.</p>
                <p><button class="btn transparent">OUR STORY</button></p>
            </div>
            <div class="col s12 m3 l3">
                <h4 class="footer-title">Latest Recipes</h4>
                <?php if (! empty ($latestRecipes)): ?>
                    <ul>
                        <?php foreach ($latestRecipes as $recipe): ?>
                            <li><?= $recipe['item_name'] ?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="col s12 m3 l3">
                <h4 class="footer-title">Featured Courses</h4>
                <div class="row">
                    <?php if (! empty($courses)): ?>
                        <?php foreach ($courses as $course): ?>
                            <div class="col s4">
                                <img src="<?= $course['course_featured_image'] ?>" class="responsive-img" alt=""/>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col s12 m3 l3">
                <h4 class="footer-title">Contact Details</h4>
                <p>I live in a paradise. Everything here is just perfect. People love each other as family.</p>
                <br/>
                <i class="fa fa-location-arrow"></i> &nbsp; Ranjhi, Jabalpur
                <br/>
                <i class="fa fa-phone"></i> &nbsp; +91 - (9827) - (9837) - (62)
                <br/>
                <i class="fa fa-envelope"></i>&nbsp; varunshrivastava007@gmail.com
            </div>
        </div>
    </div>

    <hr/>

    <!-- Footer Bottom Stripe -->
    <div class="container">
        <div class="row">
            <div class="col s6 left-align">
                &copy; Fable. Designed by <a href="http://varunshrivastava.in">Varun Shrivastava</a>
            </div>
            <div class="col s6 right-align">
                Follow Us:
                <a target="_blank" href="https://www.facebook.com/varun.shrivastava.3">
                    <i class="fa fa-facebook-square"></i>
                </a>
                <a target="_blank" href="//www.twitter.com/vs_shrivastava">
                    <i class="fa fa-twitter-square"></i>
                </a>
                <a target="_blank" href="//www.instagram.com/vs_lala">
                    <i class="fa fa-instagram"></i>
                </a>
            </div>
        </div>
    </div>
</footer>



<script src="https://code.jquery.com/jquery-2.2.2.min.js" integrity="sha256-36cp2Co+/62rEAAYHLmRCPIych47CvdM+uTBJwSzWjI=" crossorigin="anonymous"></script>
<script src="<?= _WEBROOT ?>js/main.js"></script>
</body>
</html>