<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?= $this->fetch('title') ?></title>

        <!-- Grid Css -->
        <link rel="stylesheet" href="<?= _WEBROOT ?>css/build/main.css"/>
        <link rel="stylesheet" href="<?= _WEBROOT ?>css/materialize.css"/>

        <!-- Animate Css -->
        <link rel="stylesheet" href="<?= _WEBROOT ?>css/animate.css"/>

        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"/>

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>

    </head>

    <body>