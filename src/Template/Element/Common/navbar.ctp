<nav class="top-bar">
    <div class="nav-wrapper">
        <a href="#" class="brand-logo">Logo</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li class="<?= $this->fetch('homeActive') ?>"><a href="<?= $this->request->webroot ?>home">Home</a></li>
            <li class="<?= $this->fetch('aboutActive') ?>"><a href="<?= $this->request->webroot ?>about">About Us</a></li>
            <li class="<?= $this->fetch('menuActive') ?>"><a href="<?= $this->request->webroot ?>menu">Menu</a></li>
            <li class="<?= $this->fetch('reservationActive') ?>"><a href="<?= $this->request->webroot ?>reservations">Reservations</a></li>
            <li class="<?= $this->fetch('contactActive') ?>"><a href="<?= $this->request->webroot ?>contact">Contact</a></li>
        </ul>
    </div>
</nav>