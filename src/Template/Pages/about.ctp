<section class="window about-us">
    <div class="window--image-overlay"></div>

    <!-- Center Text -->
    <div class="window__center">
        <p class="title animated bounceIn">True Passion</p>
        <p class="tagline white-text animated fadeInUp">Natural Flavours</p>
        <p class="summary white-text animated fadeInUp">Opened in February 2016, La Cuisine is the largest restaurants in Ranjhi.
            A bustling atmospheric restaurant, with a huge open kitchen.</p>
        <p>
            <i style="cursor: pointer;" id="window_discover_btn" class="fa fa-2x fa-chevron-down animated infinite swing white-text"></i>
        </p>
    </div>
</section>
<span class="window-landing-end"></span>

<div class="give-freedom"></div>

<!-- Two Column Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/a15.jpg" class="responsive-img side-image" alt=""/>
            </div>
            <div class="col s12 m6 l6">
                <article class="center-align">
                    <p class="title">Discover</p>
                    <p class="tagline">Best Recipes</p>
                    <p class="summary">Curabitur quas nets lacus ets nulat iaculis loremis etis nisle varius vitae seditum fugiatum ligula aliquam qui sequi. Lorem ipsum dolor sit amet, consectetur adipiscing elit rutrum eleif arcu sit aspernatur nets fugit, sed quia.</p>
                    <p>
                        <button class="btn btn-yellow">OUR RECIPES</button>
                    </p>
                </article>
            </div>
        </div>
    </div>
</section>

<div class="give-freedom"></div>
<!-- Two Column Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <article class="center-align">
                    <p class="title">Discover</p>
                    <p class="tagline">OUR CHEFS</p>
                    <p class="summary">Curabitur quas nets lacus ets nulat iaculis loremis etis nisle varius vitae seditum fugiatum ligula aliquam qui sequi. Lorem ipsum dolor sit amet, consectetur adipiscing elit rutrum eleif arcu sit aspernatur nets fugit, sed quia.</p>
                    <p>
                        <img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/menu-logo.png" class="responsive-img" alt=""/>
                    </p>
                </article>
            </div>
            <div class="col s12 m6 l6">
                <img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/chef1.jpg" class="responsive-img side-image" alt=""/>
            </div>
        </div>
    </div>
</section>