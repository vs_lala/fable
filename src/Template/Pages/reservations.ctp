<?php $this->assign('reservationActive', 'active'); ?>

<section class="window reservation">
    <div class="window--image-overlay"></div>
    <div class="window__center">
        <p class="title animated fadeInDown">La Cuisine</p>
        <p class="tagline white-text animated fadeInUp">Special Moments</p>
        <p class="summary white-text animated fadeInUp">An unforgettable dining experience in the heart of Jabalpur, Madhya Pradesh.
            Contemporary british menu, inspired by the traditions of Great Britain.</p>
        <p>
            <button id="window_discover_btn" class="btn btn-white transparent">Book Now!</button>
        </p>
    </div>
</section>

<div class="give-freedom"></div>

<!-- Two Column Section -->
<span class="window-landing-end"></span>
<section>
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/chef2.jpg" class="responsive-img side-img" alt=""/>
            </div>
            <div class="col s12 m6 l6">
                <article class="center-align">
                    <p class="title">Reservations</p>
                    <p class="tagline">Online Booking</p>
                    <p class="summary">Curabitur quas nets lacus ets nulat iaculis loremis etis nisle varius vitae seditum fugiatum ligula aliquam qui sequi. Lorem ipsum dolor sit amet, consectetur adipiscing elit rutrum eleif arcu sit aspernatur nets fugit, sed quia.</p>

                    <!-- Form Starts Here... -->
                    <?= $this->Form->create($reservationModel, ['url' => ['controller'=>'Reservations', 'action'=>'add']]); ?>
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <?= $this->Form->input('first_name', ['label'=>false, 'class'=>'form-control', 'placeholder'=>'First Name']); ?>
                        </div>
                        <div class="col s12 m6 l6">
                            <?= $this->Form->input('last_name', ['label'=>false, 'class'=>'form-control', 'placeholder'=>'Last Name']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <?= $this->Form->input('email', ['label'=>false, 'class'=>'form-control', 'placeholder'=>'E-Mail ID']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <?= $this->Form->input('mobile', ['label'=>false, 'class'=>'form-control', 'placeholder'=>'10 Digits Only']); ?>
                        </div>
                        <div class="col s12 m6 l6">
                            <?= $this->Form->input('people', ['label'=>false, 'class'=>'form-control', 'placeholder'=>'How Many?']); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m6 l6">
                            <label for="reservation_date">Reservation Date (yy-mm-dd)</label>
                            <?= $this->Form->input('reservation_date', ['label' => false]); ?>
                        </div>
                        <div class="col s12 m6 l6">
                            <label for="reservation_date">Reservation Time (hh-mm)</label>
                            <?= $this->Form->input('reservation_time', ['label' => false]); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <button class="btn btn-yellow" type="submit" style="width: 100%;">Book Your Table</button>
                        </div>
                    </div>
                    <?= $this->Form->end(); ?>
                </article>
            </div>
        </div>
    </div>
</section>

<div class="give-freedom"></div>

<!-- Two Column Section -->
<section>
    <div class="container">
        <div class="row">
            <div class="col s12 m6 l6">
                <article class="center-align">
                    <p class="title">Relaxing</p>
                    <p class="tagline">Atmosphere</p>
                    <p class="summary">We are here to fill your stomach 24x7. We would love to have you onboard with us. Book Online to reserve your seat with someone special. We are waiting.</p>
                    <p><button class="btn btn-yellow" onclick="window.location = '/projects/fable/gallery'">See Gallery</button></p>
                </article>
            </div>
            <div class="col s12 m6 l6">
                <img style="margin-top: 30px;" src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/food6.jpg" class="responsive-img side-image" alt=""/>
            </div>

        </div>
    </div>
</section>