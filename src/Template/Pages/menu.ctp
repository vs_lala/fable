<?php $this->assign('menuActive', 'active'); ?>

<section class="window landing">
    <div class="window--image-overlay"></div>
    <div class="window__center">
        <p class="title">Our Menu</p>
        <p class="tagline white-text">FOOD & DRINKS</p>
        <p class="summary white-text">An unforgettable dining experience in the heart of Covent Garden, London.
            Contemporary british menu, inspired by the traditions of Great Britain.</p>
    </div>
</section>

<div class="give-freedom"></div>

<section>
    <div class="container">
        <div class="row">
            <?php if (! empty ($categories)): ?>
                <?php foreach ($categories as $cat): ?>

                    <div class="col s12 m6 l6 left-align">
                        <span class="section-main-heading"><?= $cat['cat_name']; ?></span>

                        <div class="give-freedom"></div>
                        <!-- Check if courses are present -->
                        <?php if (! empty ($courses)): ?>
                            <!-- If courses are present then iterate through each course -->
                            <?php foreach ($courses as $course): ?>
                                <!-- If course id matches the corresponding category Id then print the course -->
                                <?php if ($course['cat_id'] == $cat['id']): ?>
                                    <div class="row">
                                        <div class="col s3">
                                            <img src="<?= $course['course_featured_image'] ?>" class="responsive-img" alt=""/>
                                        </div>
                                        <div class="col s9 left-align">
                                            <span class="section-heading"><?= $course['course_type'] ?></span>
                                            <p class="section-description"><?= $course['course_description'] ?></p>
                                            <p><span class="price"><?= $course['course_price'] ?></span></p>
                                        </div>
                                    </div>
                                <?php endif; ?>

                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</section>