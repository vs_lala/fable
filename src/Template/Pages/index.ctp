<?php $this->assign('homeActive', 'active'); ?>

<section class="window landing">
    <div class="window--image-overlay"></div>
    <div class="window__center">
        <p><img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/logo2.png" class="responsive-img animated fadeInDown" alt=""/></p>
        <p class="title large animated rubberBand ">Food Court</p>
        <p class="tagline white-text animated bounceIn">We are known for Great French Cooking</p>
        <p><button id="window_discover_btn" class="btn btn-transparent btn-white animated bounceInRight">Discover</button></p>
    </div>
</section>
<span class="window-landing-end"></span>

<section class="window">
    <div class="container">
        <div class="give-freedom"></div>
        <div class="row">
            <div class="col s12 m6 l6">
                <article>
                    <h3 class="title animated">Discover</h3>
                    <p class="tagline animated">our story</p>
                    <p class="animated">Curabitur quas nets lacus ets nulat iaculis loremis etis nisle varius vitae seditum fugiatum ligula aliquam qui sequi. Lorem ipsum dolor sit amet, consectetur adipiscing elit rutrum eleif arcu sit aspernatur nets fugit, sed quia.</p>
                    <p>
                        <button class="btn btn-yellow animated">Discover</button>
                    </p>
                </article>
            </div>
            <div class="col s12 m6 l6">
                <img src="http://bogdanpreda.com/themes/tf-fable/restaurant/images/food3.jpg" class="responsive-img" alt=""/>
            </div>
        </div>
    </div>
</section>