<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Menu Category'), ['action' => 'edit', $menuCategory->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu Category'), ['action' => 'delete', $menuCategory->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuCategory->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Category'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="menuCategories view large-9 medium-8 columns content">
    <h3><?= h($menuCategory->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Cat Name') ?></th>
            <td><?= h($menuCategory->cat_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($menuCategory->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created At') ?></th>
            <td><?= h($menuCategory->created_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Cat Description') ?></h4>
        <?= $this->Text->autoParagraph(h($menuCategory->cat_description)); ?>
    </div>
</div>
