<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="menuCategories form large-9 medium-8 columns content">
    <?= $this->Form->create($menuCategory) ?>
    <fieldset>
        <legend><?= __('Add Menu Category') ?></legend>
        <?php
            echo $this->Form->input('cat_name', ['label' => 'Category Title']);
            echo $this->Form->input('cat_description', ['label' => 'Category Description']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
