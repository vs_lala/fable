<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $menuItem->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $menuItem->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Menu Items'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Menu Course'), ['controller' => 'MenuCourse', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Menu Course'), ['controller' => 'MenuCourse', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="menuItems form large-9 medium-8 columns content">
    <?= $this->Form->create($menuItem) ?>
    <fieldset>
        <legend><?= __('Edit Menu Item') ?></legend>
        <?php
            echo $this->Form->input('course_id', ['options' => $menuCourse]);
            echo $this->Form->input('item_name');
            echo $this->Form->input('item_price');
            echo $this->Form->input('item_duration', ['main.js' => true]);
            echo $this->Form->input('item_description');
            echo $this->Form->input('item_featured_image');
            echo $this->Form->input('created_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
