<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Menu Item'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Menu Course'), ['controller' => 'MenuCourse', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Menu Course'), ['controller' => 'MenuCourse', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="menuItems index large-9 medium-8 columns content">
    <h3><?= __('Menu Items') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('course_id') ?></th>
                <th><?= $this->Paginator->sort('item_name') ?></th>
                <th><?= $this->Paginator->sort('item_price') ?></th>
                <th><?= $this->Paginator->sort('item_duration') ?></th>
                <th><?= $this->Paginator->sort('item_featured_image') ?></th>
                <th><?= $this->Paginator->sort('created_at') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($menuItems as $menuItem): ?>
            <tr>
                <td><?= $this->Number->format($menuItem->id) ?></td>
                <td><?= $menuItem->has('menu_course') ? $this->Html->link($menuItem->menu_course->id, ['controller' => 'MenuCourse', 'action' => 'view', $menuItem->menu_course->id]) : '' ?></td>
                <td><?= h($menuItem->item_name) ?></td>
                <td><?= $this->Number->format($menuItem->item_price) ?></td>
                <td><?= h($menuItem->item_duration) ?></td>
                <td><?= h($menuItem->item_featured_image) ?></td>
                <td><?= h($menuItem->created_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $menuItem->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menuItem->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menuItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuItem->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
