<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Menu Item'), ['action' => 'edit', $menuItem->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu Item'), ['action' => 'delete', $menuItem->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuItem->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Menu Course'), ['controller' => 'MenuCourse', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Course'), ['controller' => 'MenuCourse', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="menuItems view large-9 medium-8 columns content">
    <h3><?= h($menuItem->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Menu Course') ?></th>
            <td><?= $menuItem->has('menu_course') ? $this->Html->link($menuItem->menu_course->id, ['controller' => 'MenuCourse', 'action' => 'view', $menuItem->menu_course->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Item Name') ?></th>
            <td><?= h($menuItem->item_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Item Featured Image') ?></th>
            <td><?= h($menuItem->item_featured_image) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($menuItem->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Item Price') ?></th>
            <td><?= $this->Number->format($menuItem->item_price) ?></td>
        </tr>
        <tr>
            <th><?= __('Item Duration') ?></th>
            <td><?= h($menuItem->item_duration) ?></td>
        </tr>
        <tr>
            <th><?= __('Created At') ?></th>
            <td><?= h($menuItem->created_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Item Description') ?></h4>
        <?= $this->Text->autoParagraph(h($menuItem->item_description)); ?>
    </div>
</div>
