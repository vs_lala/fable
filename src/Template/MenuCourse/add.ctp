<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Menu Course'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['controller' => 'MenuCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Menu Category'), ['controller' => 'MenuCategories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="menuCourse form large-9 medium-8 columns content">
    <?= $this->Form->create($menuCourse) ?>
    <fieldset>
        <legend><?= __('Add Menu Course') ?></legend>

            <select name="cat_id">
                <?php foreach ($menuCategories as $category): ?>
                    <option value="<?= $category['id'] ?>"><?= $category['cat_name'] ?></option>
                <?php endforeach; ?>
            </select>

        <?php

            echo $this->Form->input('course_type');
            echo $this->Form->input('course_description');
            echo $this->Form->input('course_price');
            echo $this->Form->input('course_featured_image');
            echo $this->Form->input('created_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
