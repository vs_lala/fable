<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Menu Course'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['controller' => 'MenuCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Menu Category'), ['controller' => 'MenuCategories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="menuCourse index large-9 medium-8 columns content">
    <h3><?= __('Menu Course') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cat_id') ?></th>
                <th><?= $this->Paginator->sort('course_type') ?></th>
                <th><?= $this->Paginator->sort('course_featured_image') ?></th>
                <th><?= $this->Paginator->sort('created_at') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($menuCourse as $menuCourse): ?>
            <tr>
                <td><?= $this->Number->format($menuCourse->id) ?></td>
                <td><?= $menuCourse->has('menu_category') ? $this->Html->link($menuCourse->menu_category->id, ['controller' => 'MenuCategories', 'action' => 'view', $menuCourse->menu_category->id]) : '' ?></td>
                <td><?= h($menuCourse->course_type) ?></td>
                <td><?= h($menuCourse->course_featured_image) ?></td>
                <td><?= h($menuCourse->created_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $menuCourse->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $menuCourse->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menuCourse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuCourse->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
