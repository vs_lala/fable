<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Menu Course'), ['action' => 'edit', $menuCourse->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Menu Course'), ['action' => 'delete', $menuCourse->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuCourse->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Menu Course'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Course'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['controller' => 'MenuCategories', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Menu Category'), ['controller' => 'MenuCategories', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="menuCourse view large-9 medium-8 columns content">
    <h3><?= h($menuCourse->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Menu Category') ?></th>
            <td><?= $menuCourse->has('menu_category') ? $this->Html->link($menuCourse->menu_category->id, ['controller' => 'MenuCategories', 'action' => 'view', $menuCourse->menu_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Course Type') ?></th>
            <td><?= h($menuCourse->course_type) ?></td>
        </tr>
        <tr>
            <th><?= __('Course Featured Image') ?></th>
            <td><?= h($menuCourse->course_featured_image) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($menuCourse->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Created At') ?></th>
            <td><?= h($menuCourse->created_at) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Course Description') ?></h4>
        <?= $this->Text->autoParagraph(h($menuCourse->course_description)); ?>
    </div>
</div>
