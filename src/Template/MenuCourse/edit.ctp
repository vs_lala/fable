<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $menuCourse->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $menuCourse->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Menu Course'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Menu Categories'), ['controller' => 'MenuCategories', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Menu Category'), ['controller' => 'MenuCategories', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="menuCourse form large-9 medium-8 columns content">
    <?= $this->Form->create($menuCourse) ?>
    <fieldset>
        <legend><?= __('Edit Menu Course') ?></legend>
        <?php
            echo $this->Form->input('cat_id', ['options' => $menuCategories]);
            echo $this->Form->input('course_type');
            echo $this->Form->input('course_description');
            echo $this->Form->input('course_price');
            echo $this->Form->input('course_featured_image');
            echo $this->Form->input('created_at');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
