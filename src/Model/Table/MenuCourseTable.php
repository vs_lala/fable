<?php
namespace App\Model\Table;

use App\Model\Entity\MenuCourse;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MenuCourse Model
 *
 * @property \Cake\ORM\Association\BelongsTo $MenuCategories
 */
class MenuCourseTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('menu_course');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('MenuCategories', [
            'foreignKey' => 'cat_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('course_type', 'create')
            ->notEmpty('course_type');

        $validator
            ->allowEmpty('course_description');

        $validator
            ->allowEmpty('course_featured_image');

        $validator
            ->allowEmpty('course_price');

        $validator
            ->dateTime('created_at')
            ->requirePresence('created_at', 'create')
            ->notEmpty('created_at');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['cat_id'], 'MenuCategories'));
        return $rules;
    }
}
