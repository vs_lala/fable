<?php
namespace App\Model\Entity;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\Entity;

/**
 * MenuCategory Entity.
 *
 * @property int $id
 * @property string $cat_name
 * @property string $cat_description
 * @property \Cake\I18n\Time $created_at
 */
class MenuCategory extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];

}
