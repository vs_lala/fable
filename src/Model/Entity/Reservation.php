<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Reservation Entity.
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile
 * @property int $people
 * @property \Cake\I18n\Time $reservation_date
 * @property \Cake\I18n\Time $reservation_time
 * @property \Cake\I18n\Time $created_at
 * @property \Cake\I18n\Time $modified_at
 */
class Reservation extends Entity
{

}
