<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MenuItem Entity.
 *
 * @property int $id
 * @property int $course_id
 * @property \App\Model\Entity\MenuCourse $menu_course
 * @property string $item_name
 * @property float $item_price
 * @property \Cake\I18n\Time $item_duration
 * @property string $item_description
 * @property string $item_featured_image
 * @property \Cake\I18n\Time $created_at
 */
class MenuItem extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
