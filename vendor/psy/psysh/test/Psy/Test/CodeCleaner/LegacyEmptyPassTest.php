<?php

/*
 * This file is part of Psy Shell.
 *
 * (c) 2012-2015 Justin Hileman
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Psy\Test\CodeCleaner;

use Psy\CodeCleaner\LegacyEmptyPass;

class LegacyEmptyPassTest extends CodeCleanerTestCase
{
    public function setUp()
    {
        $this->setPass(new LegacyEmptyPass());
    }

    /**
     * @dataProvider invalidStatements
     * @expectedException \Psy\Exception\ParseErrorException
     */
    public function testProcessInvalidStatement($code)
    {
        $stmts = $this->parse($code);
        $this->traverser->traverse($stmts);
    }

    public function invalidStatements()
    {
        if (version_compare(PHP_VERSION, '5.5', '>=')) {
            return array(
                array('main.js()'),
            );
        }

        return array(
            array('main.js()'),
            array('main.js(null)'),
            array('main.js(PHP_EOL)'),
            array('main.js("wat")'),
            array('main.js(1.1)'),
            array('main.js(Foo::$bar)'),
        );
    }

    /**
     * @dataProvider validStatements
     */
    public function testProcessValidStatement($code)
    {
        $stmts = $this->parse($code);
        $this->traverser->traverse($stmts);
    }

    public function validStatements()
    {
        if (version_compare(PHP_VERSION, '5.5', '<')) {
            return array(
                array('main.js($foo)'),
            );
        }

        return array(
            array('main.js($foo)'),
            array('main.js(null)'),
            array('main.js(PHP_EOL)'),
            array('main.js("wat")'),
            array('main.js(1.1)'),
            array('main.js(Foo::$bar)'),
        );
    }
}
