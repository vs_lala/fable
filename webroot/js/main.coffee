$ ->
  $('#window_discover_btn').click (e) ->
    $('html,body').animate
      scrollTop: $('.window-landing-end').offset().top

# Cache Reference to window animation elements
$animatedElements = $('.animated')
$window = $(window)
$window.on('scroll resize', checkIfInView());

# checking if the elements is in view
checkIfInView =  ->
  windowHeight = $window.height()
  windowTopPosition = $window.scrollTop()
  windowBottomPosition = windowTopPosition + windowHeight

  $.each($animatedElements, () ->
    $element = $(this)
    console.log($element)
    elementHeight = $element.outerHeight()
    elementTopPosition = $element.offset().top
    elementBottomPosition = elementTopPosition + elementHeight

    # check to see if the current element is in the viewport
    if ((elementBottomPosition >= windowTopPosition) && (elementTopPosition <= windowBottomPosition))
      $element.addClass('bounceIn')
    else
      $element.removeClass('bounceIn')
  )

