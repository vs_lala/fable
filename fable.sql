-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 12:53 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 7.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fable`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_categories`
--

CREATE TABLE IF NOT EXISTS `menu_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(100) NOT NULL,
  `cat_description` text,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_name` (`cat_name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_categories`
--

INSERT INTO `menu_categories` (`id`, `cat_name`, `cat_description`, `created_at`) VALUES
(2, 'Breakfast', 'Served in the Morning', '2016-03-28 14:22:00'),
(6, 'Lunch', 'Served at Afternoon', '2016-03-28 14:41:38');

-- --------------------------------------------------------

--
-- Table structure for table `menu_course`
--

CREATE TABLE IF NOT EXISTS `menu_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `course_type` varchar(200) NOT NULL,
  `course_price` double DEFAULT NULL,
  `course_description` text,
  `course_featured_image` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_course`
--

INSERT INTO `menu_course` (`id`, `cat_id`, `course_type`, `course_price`, `course_description`, `course_featured_image`, `created_at`) VALUES
(1, 6, 'Sahi Indian', 2999, 'Sahi or Shahi is a clan of Jats found in Punjab region of Pakistan and India. The Jat community saw radical social changes in the 17th century, the Hindu Jats took up arms against the Mughal Empire during the late 17th and early 18th century. ', 'http://bogdanpreda.com/themes/tf-fable/restaurant/images/m1.jpg', '2016-03-30 18:58:00'),
(2, 2, 'Great British Breakfast', 1299, 'A full breakfast is a breakfast meal, usually including bacon, sausages, eggs, and a variety of other cooked foods, with a beverage such as coffee or tea. It is especially popular in the UK and Ireland, to the extent that many cafés and pubs offer the meal at any time of day as an "all-day breakfast"', 'http://25.media.tumblr.com/tumblr_mcxvlzft9j1qdmguqo1_500.jpg', '2016-03-31 04:05:00'),
(3, 2, 'Punjabi Special Breakfast', 1999, 'Punjabi foods are rich calorie food which is meant to suit their lifestyle as they work whole day in the field in the rural areas. Some of the dishes which are just meant for the Punjabi are Mah Di Dal and Sarso Da Saag. The main masala in Punjabi dishes is made from Ginger, Onion and Garlic.', 'http://d30wzlxgiwic4.cloudfront.net/image/restaurant/photos/327/473/000/000473327_size_3.jpg?1454917', '2016-03-31 04:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE IF NOT EXISTS `menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `item_price` decimal(10,0) NOT NULL,
  `item_duration` time DEFAULT NULL,
  `item_description` text,
  `item_featured_image` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `course_id`, `item_name`, `item_price`, `item_duration`, `item_description`, `item_featured_image`, `created_at`) VALUES
(1, 1, 'Chicken Tandoori', '599', '20:00:00', 'Tandoori chicken is a dish originating in the Indian subcontinent. It is widely popular in South Asia, Malaysia, Singapore, Indonesia, the Middle East and the Western world. It consists of roasted chicken prepared with yogurt and spices.', 'http://aramkitchen.com/wp-content/uploads/2013/09/legfinal1-piece-short.jpg', '2016-03-30 19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `page_info`
--

CREATE TABLE IF NOT EXISTS `page_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(100) NOT NULL,
  `page_title` varchar(500) NOT NULL,
  `page_summary` varchar(1000) DEFAULT NULL,
  `page_description` text,
  `page_featured_image` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `people` int(11) NOT NULL,
  `reservation_date` date NOT NULL,
  `reservation_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `first_name`, `last_name`, `email`, `mobile`, `people`, `reservation_date`, `reservation_time`, `created_at`, `modified_at`) VALUES
(1, 'Varun', 'Shrivastava', '', '9827983762', 4, '2016-03-31', '10:02:00', '2016-03-31 10:06:16', '2016-03-31 10:06:16');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu_course`
--
ALTER TABLE `menu_course`
  ADD CONSTRAINT `FK_COURSE_CATEGORY` FOREIGN KEY (`cat_id`) REFERENCES `menu_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `FK_MENU_COURSE` FOREIGN KEY (`course_id`) REFERENCES `menu_course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
